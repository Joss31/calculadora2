
public class Client {
	public static void main(String[] args){
		Calc calc = new Calc();
		int result = calc.operate("+", 2, 5);
		System.out.println(result);
		result = calc.operate("-", 5, 3);
		System.out.println(result);
		result = calc.operate("*", 5, 3);
		System.out.println(result);
		result = calc.operate("@", 2, 4);
		System.out.println(result);
	}
}

class Multiplicacion extends Operation{
	public int operate(int a, int b){
		return a*b;
	}
}

class Division extends Operation{
	public int operate(int a, int b){
		return a/b;
	}
}
